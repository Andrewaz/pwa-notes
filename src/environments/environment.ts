// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAreFQVEm84brGrHEKoY8WsCstxFUZDu34',
    authDomain: 'aws-pwa.firebaseapp.com',
    databaseURL: 'https://aws-pwa.firebaseio.com',
    projectId: 'aws-pwa',
    storageBucket: 'aws-pwa.appspot.com',
    messagingSenderId: '666672597040',
    appId: '1:666672597040:web:ce4c246672febc6c6105f5'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
