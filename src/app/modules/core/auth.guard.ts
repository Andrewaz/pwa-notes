import { Injectable } from '@angular/core';
import { Router, CanLoad } from '@angular/router';
import { Observable, of } from 'rxjs';
import {FirebaseAuthService} from './firebase-auth-service.service';

@Injectable()
export class AuthGuard implements CanLoad {
  constructor(private auth: FirebaseAuthService, private router: Router) {}

  canLoad(): Observable<boolean> {
    if (!this.auth.authenticated) {
      this.router.navigate(['/user']);
      return of(false);
    }
    return of(true);
  }
}
