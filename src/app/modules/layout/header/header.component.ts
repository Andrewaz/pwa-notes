import {Component, OnInit} from '@angular/core';
import {FirebaseAuthService} from '../../core/firebase-auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
  public user$ = this.auth.user$;

  constructor(private auth: FirebaseAuthService) {
  }
}
